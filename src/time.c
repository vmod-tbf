/* This file is part of vmod-tbf
   Copyright (C) 2013-2017 Sergey Poznyakoff
  
   Vmod-tbf is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Vmod-tbf is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with vmod-tbf.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "tbf.h"
#include <time.h>

VCL_INT
vmod_systime(VRT_CTX)
{
	return time(NULL);
}

VCL_STRING
vmod_strftime(VRT_CTX, VCL_STRING format, VCL_INT timestamp)
{
	time_t ts = (time_t) timestamp;
	size_t u, n;
	char *p;
	
	u = WS_ReserveAll(ctx->ws);
        p = ctx->ws->f;
        n = strftime(p, u, format, gmtime(&ts));
	if (n == 0) {
		WS_Release(ctx->ws, 0);
		return NULL;
	}

	WS_Release(ctx->ws, n + 1);

	return p;
}
