.\" This file is part of Vmod-tbf -*- nroff -*-
.\" Copyright (C) 2013-2022 Sergey Poznyakoff
.\"
.\" Vmod-tbf is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Vmod-tbf is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with vmod-tbf.  If not, see <http://www.gnu.org/licenses/>.
.TH VMOD-TBF 3 "August 21, 2022" "VMOD-TBF" "User Reference"
.SH NAME
vmod-tbf \- token bucket filtering for Varnish
.SH SYNOPSIS
.B import tbf;

.BI "BOOL tbf.rate(STRING " key ", INT " COST ", DURATION " interval ", INT " burst_size ");"

.BI "BOOL tbf.check(STRING " key ", STRING " rate ");"

.BI "VOID tbf.dump(STRING " file ");"

.BI "VOID tbf.dump_at_exit(STRING " file ");"

.BI "VOID tbf.load(STRING " file ");"

.BI "VOID tbf.gc(DURATION " interval ");"

.BI "VOID tbf.set_gc_interval(DURATION " interval ");"

.BI "VOID tbf.debug(INT " level ");"

.BI "VOID tbf.log_tree(INT " prio ");"

.BI "VOID tbf.log_stats(INT " prio ");"

.BI "REAL tbf.getla(INT " sample ");"

.BI "INT tbf.systime();"

.BI "STRING tbf.strftime(STRING " format ", INT " timestamp ");"

.BI "VOID tbf.sleep(DURATION " time ");"

.SH DESCRIPTION
The
.B vmod-tbf
module implements \fBtoken bucket filtering\fR for
.BR "Varnish Cache" .
It also implements several unrelated functions.
.SS Rate control functions
.PP
In this implementation of the token bucket algorithm, each bucket is
associated with a \fBkey\fR (a string value uniquely identifying this
bucket).  The algorithm works as follows:
.TP
.B \(bu
A token is added to the bucket at a constant rate of 1 token
per \fIinterval\fR microseconds.
.TP
.B \(bu
A bucket can hold at most
\fIburst_size\fR tokens.  Any tokens arriving when the bucket is full
are discarded.
.TP
.B \(bu
When \fIcost\fR items of data arrive, \fIcost\fR tokens are removed
from the bucket and the data are accepted.
.TP
.B \(bu
If fewer than \fIcost\fR tokens are available, no tokens are removed from
the bucket and the data are not accepted.
.PP
This keeps the data traffic at a constant rate or \fIcost\fR items
per \fIinterval\fR microseconds with bursts of up to \fIburst_size\fR
items.  Such bursts occur when no data was being arrived for
\fIburst_size*interval\fR or more microseconds.
.PP
The function \fBtbf.rate\fR provides a low-level interface to this
algorithm.  Its arguments correspond exactly to the values used in
the above description.  The \fBkey\fR argument identifies the bucket.
The function returns \fBTRUE\fR if the data are accepted and
\fBFALSE\fR otherwise.  The sample usage is:
.PP
.EX
sub vcl_recv {
    if (!tbf.rate("ip:" + client.ip, 1, 0.1s, 20)) {
        error(429, "Request rate exceeded");
    }
}
.EE 
.PP
This example will keep the incoming requests at the rate of 10 requests
per second, allowing for bursts of up to 20 requests after each 2
second (or longer) period of inactivity.
.PP
For VCL 4.0, replace
.EX
        error(429, "Request rate exceeded");
.EE
with
.EX
        return(synth(429, "Request rate exceeded"));
.EE
.PP
The \fBtbf.check\fR function provides a higher-level interface.  Its
first argument identifies the bucket.  The \fIrate\fR argument is a
textual rate specification in the form:
.PP
.BI "" N req/ K "" U
.PP
where \fIN\fR and \fIK\fR are floating-point numbers, and \fIU\fR is
an optional unit specifier: \fBs\fR, \fBm\fR, \fBh\fR or \fBd\fR for
seconds, minutes, hours or days, correspondingly.  The parts of the
rate specification can be separated by any amount of whitespace.
.PP
For example, the following statement will limit the request rate to
ten and a half requests per second:
.PP
.EX
sub vcl_recv {
    if (!tbf.check(client.ip, "10.5 req/1s")) {
        error(429, "Request rate exceeded");
    }
}
.EE
.SS Storage
.PP
Buckets are kept in a binary tree structure, indexed by the key value.
In order to prevent stale data from accumulating in the memory,
garbage collection is run periodically.  During garbage collection,
all buckets that have not been accessed more than predefined amount of
time are removed from the tree and disposed of.  The default interval
is 1 hour.  It can be configured using the
.B tbf.set_gc_interval
function.
.PP
Garbage collection can also be triggered explicitly, using the
.B tbf.gc
function with the interval as its argument.
.PP
The function
.B tbf.dump
dumps entire tree to the disk file.  The function
.B tbf.load
loads the named dump file into the tree.
.PP
The function
.B tbf.dump_at_exit 
configures the module to dump the tree before
.B varnishd
terminates.  Use this to keep the \fBtbf\fR state across
\fBvarnishd\fR restarts.  For example:
.PP
.EX
sub vcl_init {
    tbf.load("/var/run/tbf.dump");
    tbf.dump_at_exit("/var/run/tbf.dump");
}
.EE
.SS Debugging
.TP
.BI "VOID tbf.debug(INT " level ");"
Sets the debug verbosity level.  Meaningful argument values are
\fB0\fR (no debugging info) to \fB2\fR (maximum verbosity).
.TP
.BI "VOID tbf.log_tree(INT " prio ");"
Logs the content of the \fBtbf\fR tree to \fBsyslog\fR priority
\fIprio\fR.  Note that this can produce extremely big amounts of info.
.TP
.BI "VOID tbf.log_stats(INT " prio ");"
Logs the tree statistics to \fBsyslog\fR priority \fIprio\fR.  The
following information is logged: total number of nodes in tree, number
of leaves, shortest, longest and average paths.
.SS Other functions
.PP
Several functions are provided that do not exactly belong to the
TBF algorithm, but which may come useful when implementing rate
control.
.TP
.BI "REAL tbf.getla(INT " sample ");"
Returns the system load average.  The argument identifies interval
for which to compute it: 1, 5 or 15 minutes.
.TP
.BI "INT tbf.systime();"
Returns the current time of day as the number of seconds since the
Epoch (1970-01-01 00:00:00 UTC). 
.TP
.BI "STRING tbf.strftime(STRING " format ", INT " timestamp ");"
Function formats the \fItimestamp\fR according to the specification in
\fIformat\fR.  See 
.BR strftime (3),
for a description of available formats.  For example, the following
statement assigns the current year to the \f(CWX\-Year\fR header:

.EX
set req.http.X-Year = tbf.strftime("%Y", systime());
.EE
.TP
.BI "VOID tbf.sleep(DURATION " time ");"
Suspends execution for a specified amount of time.
.\"
.\" The MANCGI variable is set by man.cgi script on Ulysses.
.\" The download.inc file contains the default DOWNLOAD section
.\" for man-based doc pages.
.if "\V[MANCGI]"WEBDOC" \{\
.	ds package vmod-tbf
.	ds version 2.6
.	so download.inc
\}
.SH "SEE ALSO"
.BR vcl (7),
.BR varnishd (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2013-2022 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
